import logo from './logo.svg';
import './App.css';
import {auth, googleProvider} from './firebase';
import { useEffect, useState } from 'react';
function App() {
  const [user, setUser] = useState(null);
  const signInGoogle= () => {
      auth.signInWithPopup(googleProvider)
      .then((response)=>{
        console.log(response);
        setUser(response.user);
      }).catch((error)=>{
        console.log(error);
      })
  }
  const signOutGoogle = () => {
    auth.signOut().then(()=>{
      setUser(null);
    }).catch((error)=>{
      console.log(error);
    });
  };
  useEffect(() => {
    auth.onAuthStateChanged((response)=>{
      setUser(response);
    })
  }, []);
  return (

    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {user ? <><p>Hello, {user.displayName}</p><button onClick={signOutGoogle}>Sign out</button></> : <><p>
          Please sign in.
        </p>
        <button onClick={signInGoogle}>Sign in with Google</button></>}
        
      </header>
    </div>
  );
}

export default App;
